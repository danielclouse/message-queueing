﻿using Microsoft.Azure.ServiceBus;
using Simple_Send;
using System;
using System.Text;
using System.Threading.Tasks;

public class Program
{
    private static IQueueClient queueClient;
    private const string ServiceBusConnectionString =
        "Endpoint=sb://varuslunchandlearn.servicebus.windows.net/;SharedAccessKeyName=Send;SharedAccessKey=O+ysARg/i+Ud0bN5NSnnHgVUmm8upqhlgKbR+gIEO3s=;";
    private const string QueueName = "foodtruck";

    public static void Main(string[] args)
    {
        MainAsync(args).GetAwaiter().GetResult();
        Console.WriteLine("Press any key to exit.");
        Console.ReadLine();
    }

    private static async Task MainAsync(string[] args)
    {
        queueClient = new QueueClient(ServiceBusConnectionString, QueueName, ReceiveMode.PeekLock);

        await SendMessagesToQueue();

        // Close the client after the ReceiveMessages method has exited. 
        await queueClient.CloseAsync();

    }

    // Creates a Queue client and sends 10 messages to the queue. 
    private static async Task SendMessagesToQueue()
    {
        do
        {
            try
            {
                // Create a new brokered message to send to the queue 
                var message = new Message(Encoding.UTF8.GetBytes($"{Events.RandomElement()}"));

                // Write the body of the message to the console 
                Console.WriteLine($"Sending message: {Encoding.UTF8.GetString(message.Body)}");

                // Send the message to the queue 
                await queueClient.SendAsync(message);
            }
            catch (Exception exception)
            {
                Console.WriteLine($"{DateTime.Now} > Exception: {exception.Message}");
            }

            // Delay by 10 milliseconds so that the console can keep up 
            await Task.Delay(1000);
        } while (true);


    }

    private static string[] Events = {
        "Customer Arrived",
        "Customer Departed",
        "Customer Paid",
        "Customer Ordered Meal",
        "Order sent to kitchen",
        "Cancel this order",
    };

}