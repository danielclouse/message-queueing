﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common
{
    public enum GuestStatus { Arrived, Seated, Ordered, Served, Departed }
    public class Guest
    {
        public GuestStatus Status { get; set; }
        public int TableNumber { get; set; }
        public MealOrder Order { get; set; }
    }
}
