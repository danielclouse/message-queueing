﻿using System;

namespace Common
{
    public enum MealOrderStatus { Ordered, Rejected, Cooking, OrderUp, Delivered }
    public class MealOrder
    {
        public string Description { get; set; }
        public MealOrderStatus Status { get; set; }
    }
}
