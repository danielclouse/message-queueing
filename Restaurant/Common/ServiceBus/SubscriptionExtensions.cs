﻿

using System.Threading;

namespace Common.ServiceBus
{
    using System;
    using System.Threading.Tasks;
    using Microsoft.Azure.ServiceBus;

    public static class SubscriptionExtensions
    {

        public static void RegisterMessageHandlerWithDefaultOptions(
            this ISubscriptionClient client, Func<Message, CancellationToken, Task> handler)
        {
            client.RegisterMessageHandler(handler, GetMessageHandlerOptions());
        }

        public static MessageHandlerOptions GetMessageHandlerOptions()
        {
            return new MessageHandlerOptions(ex =>
            {
                Console.WriteLine(ex.Exception.ToString(), ex.Exception.Message);
                return Task.FromException(ex.Exception);
            })
            {
                AutoComplete = false,
                MaxConcurrentCalls = 1
            };
        }
    }
}
