﻿using Common;
using Common.ServiceBus;
using Microsoft.Azure.ServiceBus;
using Newtonsoft.Json;
using ServiceBus.MessagePublisher;
using System;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

public class Program
{
    private static IMessagePublisher _messagePublisher;
    private static ISubscriptionClient _subscriptionClient;
    private const string ServiceBusListenConnectionString =
        "Endpoint=sb://varuslunchandlearn.servicebus.windows.net/;SharedAccessKeyName=Listen;SharedAccessKey=pEDvSvfGmFbWVd1VzHELIrczfgbsxen7w4CI39m14IE=;";
    private const string ServiceBusSendConnectionString =
        "Endpoint=sb://varuslunchandlearn.servicebus.windows.net/;SharedAccessKeyName=Send;SharedAccessKey=dXDE5Ke8PRao91u3sXs+ZSPWX8YkV/Fm5vOhwQCpC8o=;";
    private const string TopicName = "Restaurant";
    private const string SubscriptionName = "Guest";

    public static Guest Me = new Guest { Status = GuestStatus.Arrived };

    public static void Main(string[] args)
    {
        MainAsync().GetAwaiter().GetResult();
    }

    private static async Task MainAsync()
    {


        var sendToTopicClient = new TopicClient(ServiceBusSendConnectionString, TopicName);
        _messagePublisher = new MessagePublisher(sendToTopicClient);

        _subscriptionClient = new SubscriptionClient(ServiceBusListenConnectionString, TopicName, SubscriptionName);

        Console.WriteLine("I'm a Guest headed out to eat at this imaginary restaurant.  Press any key to quit.");

        await DineAtRestaurant();

        Console.ReadKey();
        // Close the client after the ReceiveMessages method has exited. 
        await sendToTopicClient.CloseAsync();
    }

    // Receives messages from the queue in a loop 
    private static async Task DineAtRestaurant()
    {
        try
        {
            //handle my status change
            _subscriptionClient.RegisterMessageHandlerWithDefaultOptions(HandleHavingADiningExperience);



        }
        catch (Exception exception)
        {
            Console.WriteLine($"{DateTime.Now} > Exception: {exception.Message}");
        }

        //kick the whole thing off by showing up
        Console.WriteLine("Arriving...");
        await _messagePublisher.PublisherAsync(Me);


    }

    private static async Task HandleHavingADiningExperience(Message message, CancellationToken token)
    {
        if (message.UserProperties["MessageType"].Equals("Guest"))
        {
            await UpdateMyStatus(message);
        }
        else if (message.UserProperties["MessageType"].Equals("MealOrder"))
        {
            await WatchForMyMeal(message, token);
        }
        else
        {
            Console.WriteLine("===================================================================================");
            Console.WriteLine("The guest received a message they didn't know how to handle.");
            Console.WriteLine("===================================================================================");
            Console.WriteLine(Encoding.UTF8.GetString(message.Body));
            Console.WriteLine("===================================================================================");
        }

    }

    private static async Task WatchForMyMeal(Message message, CancellationToken token)
    {
        var order = JsonConvert.DeserializeObject<MealOrder>(Encoding.UTF8.GetString(message.Body));

        switch (order.Status)
        {
            case MealOrderStatus.Ordered:
                Console.WriteLine($"I have ordered {order.Description}");
                break;
            case MealOrderStatus.Delivered:
                Console.WriteLine($"My order for {order.Description} is here.  Yum.");
                //eat the meal
                await Task.Delay(2000, token);
                Console.WriteLine($"My {order.Description} was delicious.  Paying the bill.");

                //leave 
                Console.WriteLine("Headed home for a nap.");
                await Task.Delay(2000, token);
                Me.Status = GuestStatus.Departed;
                await _messagePublisher.PublisherAsync(Me);
                break;

            case MealOrderStatus.Rejected:
                break;
            case MealOrderStatus.Cooking:
                break;
            case MealOrderStatus.OrderUp:
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }
        await _subscriptionClient.CompleteAsync(message.SystemProperties.LockToken);
    }

    private static async Task UpdateMyStatus(Message message)
    {
        var guest = JsonConvert.DeserializeObject<Guest>(Encoding.UTF8.GetString(message.Body));
        if (Me.Status != guest.Status)
        {
            Console.WriteLine($"My status has changed, I am {guest.Status}.");
            Me.Status = guest.Status;
            if (guest.Status == GuestStatus.Seated)
            {
                Me.TableNumber = guest.TableNumber;
            }
        }

        await _subscriptionClient.CompleteAsync(message.SystemProperties.LockToken);
    }
}
