﻿using Common;
using Common.ServiceBus;
using Microsoft.Azure.ServiceBus;
using Newtonsoft.Json;
using ServiceBus.MessagePublisher;
using System;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

public class Program
{
    private static IMessagePublisher _messagePublisher;
    private static ISubscriptionClient _subscriptionClient;
    private const string ServiceBusListenConnectionString =
        "Endpoint=sb://varuslunchandlearn.servicebus.windows.net/;SharedAccessKeyName=Listen;SharedAccessKey=pEDvSvfGmFbWVd1VzHELIrczfgbsxen7w4CI39m14IE=;";
    private const string ServiceBusSendConnectionString =
        "Endpoint=sb://varuslunchandlearn.servicebus.windows.net/;SharedAccessKeyName=Send;SharedAccessKey=dXDE5Ke8PRao91u3sXs+ZSPWX8YkV/Fm5vOhwQCpC8o=;";
    private const string TopicName = "Restaurant";
    private const string SubscriptionName = "Host";

    public static void Main(string[] args)
    {
        MainAsync().GetAwaiter().GetResult();
    }

    private static async Task MainAsync()
    {

        var sendToTopicClient = new TopicClient(ServiceBusSendConnectionString, TopicName);
        _messagePublisher = new MessagePublisher(sendToTopicClient);
        _subscriptionClient = new SubscriptionClient(ServiceBusListenConnectionString, TopicName, SubscriptionName);

        Console.WriteLine("I'm a restaurant host and I'm awaiting guests.  Press any key to quit.");

        SeatTheGuests();

        Console.ReadKey();
        // Close the client after the ReceiveMessages method has exited. 
        await sendToTopicClient.CloseAsync();
    }

    // Receives messages from the queue in a loop 
    private static void SeatTheGuests()
    {
        try
        {
            _subscriptionClient.RegisterMessageHandlerWithDefaultOptions(HandleMessage);
        }
        catch (Exception exception)
        {
            Console.WriteLine($"{DateTime.Now} > Exception: {exception.Message}");
        }
    }

    private static async Task HandleMessage(Message message, CancellationToken token)
    {
        if (!message.UserProperties["MessageType"].Equals("Guest"))
        {
            Console.WriteLine("The host is listening to things besides guests.");
            return;
        }
        var guest = JsonConvert.DeserializeObject<Guest>(Encoding.UTF8.GetString(message.Body));

        switch (guest.Status)
        {
            case GuestStatus.Arrived:
                //take a minute to cook the food, update a database, etc
                Console.WriteLine("A new guest has arrived, showing them to a free table.");
                await Task.Delay(5000, token);
                guest.TableNumber = GetAvailableTable();
                guest.Status = GuestStatus.Seated;
                // now that the guest is seated, let a server know they have a table
                Console.WriteLine($"Hey server - you have customers at table {guest.TableNumber}.");
                await Task.Delay(5000, token);
                await _messagePublisher.PublisherAsync(guest);
                break;
            case GuestStatus.Departed:
                //take a minute to cook the food, update a database, etc
                Console.WriteLine("Hope you had a lovely meal! Visit again soon.");
                break;

            case GuestStatus.Seated:
                break;
            case GuestStatus.Ordered:
                break;
            case GuestStatus.Served:
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }
        await _subscriptionClient.CompleteAsync(message.SystemProperties.LockToken);



    }


    private static int GetAvailableTable()
    {
        return new[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 }.RandomElement();
    }
}
