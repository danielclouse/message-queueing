﻿using Common;
using Common.ServiceBus;
using Microsoft.Azure.ServiceBus;
using Newtonsoft.Json;
using ServiceBus.MessagePublisher;
using System;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

public class Program
{
    private static IMessagePublisher _messagePublisher;
    private static ISubscriptionClient _subscriptionClient;
    private const string ServiceBusListenConnectionString =
        "Endpoint=sb://varuslunchandlearn.servicebus.windows.net/;SharedAccessKeyName=Listen;SharedAccessKey=pEDvSvfGmFbWVd1VzHELIrczfgbsxen7w4CI39m14IE=;";
    private const string ServiceBusSendConnectionString =
        "Endpoint=sb://varuslunchandlearn.servicebus.windows.net/;SharedAccessKeyName=Send;SharedAccessKey=dXDE5Ke8PRao91u3sXs+ZSPWX8YkV/Fm5vOhwQCpC8o=;";
    private const string TopicName = "Restaurant";
    private const string SubscriptionName = "Server";

    public static void Main(string[] args)
    {
        MainAsync().GetAwaiter().GetResult();
    }

    private static async Task MainAsync()
    {

        var sendToTopicClient = new TopicClient(ServiceBusSendConnectionString, TopicName);
        _messagePublisher = new MessagePublisher(sendToTopicClient);
        _subscriptionClient = new SubscriptionClient(ServiceBusListenConnectionString, TopicName, SubscriptionName);

        Console.WriteLine("I'm a restaurant server and I'm awaiting seated guests.  Press any key to quit.");

        WaitOnGuests();

        Console.ReadKey();
        // Close the client after the ReceiveMessages method has exited. 
        await sendToTopicClient.CloseAsync();
    }

    // Receives messages from the queue in a loop 
    private static void WaitOnGuests()
    {
        try
        {
            _subscriptionClient.RegisterMessageHandlerWithDefaultOptions(HandleGuestsAndOrders);


        }
        catch (Exception exception)
        {
            Console.WriteLine($"{DateTime.Now} > Exception: {exception.Message}");
        }
    }

    private static async Task HandleGuestsAndOrders(Message message, CancellationToken token)
    {
        if (message.UserProperties["MessageType"].Equals("Guest"))
        {
            await WaitOnTheGuest(message, token);
        }
        else if (message.UserProperties["MessageType"].Equals("MealOrder"))
        {
            await WaitOnTheOrder(message, token);
        }
        else
        {
            Console.WriteLine("===================================================================================");
            Console.WriteLine("The server received a message they didn't know how to handle.");
            Console.WriteLine("===================================================================================");
            Console.WriteLine(Encoding.UTF8.GetString(message.Body));
            Console.WriteLine("===================================================================================");
        }
    }

    private static async Task WaitOnTheOrder(Message message, CancellationToken token)
    {
        var order = JsonConvert.DeserializeObject<MealOrder>(Encoding.UTF8.GetString(message.Body));

        switch (order.Status)
        {
            case MealOrderStatus.OrderUp:
                //the meal is read from the kitchen
                Console.WriteLine($"One order of {order.Description} just like you asked for.");
                await Task.Delay(5000, token);

                order.Status = MealOrderStatus.Delivered;
                // now that the order is done, yell "Order Up!" and send it out
                await _messagePublisher.PublisherAsync(order);
                await _subscriptionClient.CompleteAsync(message.SystemProperties.LockToken);
                break;
            case MealOrderStatus.Ordered:
                break;
            case MealOrderStatus.Rejected:
                break;
            case MealOrderStatus.Cooking:
                break;
            case MealOrderStatus.Delivered:
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }
    }

    private static async Task WaitOnTheGuest(Message message, CancellationToken token)
    {
        var guest = JsonConvert.DeserializeObject<Guest>(Encoding.UTF8.GetString(message.Body));

        switch (guest.Status)
        {
            case GuestStatus.Seated:
                //Once the guests are seated, take their order
                Console.WriteLine("A new guest has been seated, going to take their order.");
                await Task.Delay(5000, token);

                //take their order down
                var order = new MealOrder
                {
                    Description = GetMealDescription(),
                    Status = MealOrderStatus.Ordered,
                };

                guest.Order = order;
                guest.Status = GuestStatus.Ordered;
                // now that the guest is seated, let a server know they have a table
                Console.WriteLine($"NEW ORDER! Hey cooks, you have a new order for {order.Description}.");
                await Task.Delay(5000, token);
                await _messagePublisher.PublisherAsync(guest);
                await _messagePublisher.PublisherAsync(order);
                break;

            case GuestStatus.Arrived:
                break;
            case GuestStatus.Ordered:
                break;
            case GuestStatus.Served:
                break;
            case GuestStatus.Departed:
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }
        await _subscriptionClient.CompleteAsync(message.SystemProperties.LockToken);
    }


    private static string GetMealDescription()
    {
        return new[] {
        "Bread and butter",
        "Toast and jam",
        "Mashed potatoes",
        "T-bone steaks",
        "Peanut butter",
        "Chicken and dumplings",
        }.RandomElement();
    }

}
