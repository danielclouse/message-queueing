﻿using Common;
using Microsoft.Azure.ServiceBus;
using Newtonsoft.Json;
using System;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Common.ServiceBus;
using ServiceBus.MessagePublisher;

public class Program
{
    private static IMessagePublisher _messagePublisher;
    private static ISubscriptionClient _subscriptionClient;
    private const string ServiceBusListenConnectionString =
        "Endpoint=sb://varuslunchandlearn.servicebus.windows.net/;SharedAccessKeyName=Listen;SharedAccessKey=pEDvSvfGmFbWVd1VzHELIrczfgbsxen7w4CI39m14IE=;";
    private const string ServiceBusSendConnectionString =
        "Endpoint=sb://varuslunchandlearn.servicebus.windows.net/;SharedAccessKeyName=Send;SharedAccessKey=dXDE5Ke8PRao91u3sXs+ZSPWX8YkV/Fm5vOhwQCpC8o=;";
    private const string TopicName = "Restaurant";
    private const string SubscriptionName = "Cook";

    public static void Main(string[] args)
    {
        MainAsync().GetAwaiter().GetResult();
    }

    private static async Task MainAsync()
    {

        var sendToTopicClient = new TopicClient(ServiceBusSendConnectionString, TopicName);
        _messagePublisher = new MessagePublisher(sendToTopicClient);
        _subscriptionClient = new SubscriptionClient(ServiceBusListenConnectionString, TopicName, SubscriptionName);

        Console.WriteLine("I'm a cook and I'm awaiting orders.  Press any key to quit.");

        TakeOrdersFromServers();

        Console.ReadKey();
        // Close the client after the ReceiveMessages method has exited. 
        await sendToTopicClient.CloseAsync();
    }

    // Receives messages from the queue in a loop 
    private static void TakeOrdersFromServers()
    {
        try
        {
            _subscriptionClient.RegisterMessageHandlerWithDefaultOptions(HandleMessage);
        }
        catch (Exception exception)
        {
            Console.WriteLine($"{DateTime.Now} > Exception: {exception.Message}");
        }
    }


    private static async Task HandleMessage(Message message, CancellationToken token)
    {
        if (!message.UserProperties["MessageType"].Equals("MealOrder"))
        {
            Console.WriteLine("The kitchen is listening to guests.");
            return;
        }
        var order = JsonConvert.DeserializeObject<MealOrder>(Encoding.UTF8.GetString(message.Body));

        switch (order.Status)
        {
            case MealOrderStatus.Ordered:
                //take a minute to cook the food, update a database, etc
                Console.WriteLine($"Somebody placed an order for {order.Description}.  On it.");
                await Task.Delay(5000, token);
                order.Status = MealOrderStatus.OrderUp;
                // now that the order is done, yell "Order Up!" and send it out
                Console.WriteLine($"Order up - {order.Description}.");
                await _messagePublisher.PublisherAsync(order);
                break;

            case MealOrderStatus.Rejected:
                break;
            case MealOrderStatus.Cooking:
                break;
            case MealOrderStatus.OrderUp:
                break;
            case MealOrderStatus.Delivered:
                break;
            default:
                return;
        }
        await _subscriptionClient.CompleteAsync(message.SystemProperties.LockToken);



    }
}
